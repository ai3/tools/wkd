package main

import (
	"flag"
	"io/ioutil"
	"log"

	"git.autistici.org/ai3/go-common/serverutil"
	"git.autistici.org/ai3/tools/wkd"
	"gopkg.in/yaml.v3"
)

var (
	addr       = flag.String("addr", ":5057", "address to listen on")
	configFile = flag.String("config", "/etc/wkd.yml", "path of config file")
	staticKeys = flag.String("static-keys-json", "", "file with static WKD dataset (JSON-encoded)")
)

type config struct {
	LDAP struct {
		URI    string `yaml:"uri"`
		BindDN string `yaml:"bind_dn"`
		BindPw string `yaml:"bind_pw"`
		BaseDN string `yaml:"base_dn"`
		Filter string `yaml:"filter"`
	} `yaml:"ldap"`
	HTTP *serverutil.ServerConfig `yaml:"http_server"`
}

// Read the YAML configuration file.
func loadConfig() (*config, error) {
	data, err := ioutil.ReadFile(*configFile)
	if err != nil {
		return nil, err
	}
	var c config
	if err := yaml.Unmarshal(data, &c); err != nil {
		return nil, err
	}
	return &c, nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	conf, err := loadConfig()
	if err != nil {
		log.Fatal(err)
	}

	var storages []wkd.Storage

	ldapStorage, err := wkd.NewLDAPStorage(
		conf.LDAP.URI,
		conf.LDAP.BindDN,
		conf.LDAP.BindPw,
		conf.LDAP.BaseDN,
		conf.LDAP.Filter)
	if err != nil {
		log.Fatal(err)
	}
	storages = append(storages, ldapStorage)

	if *staticKeys != "" {
		staticStorage, err := wkd.NewStaticStorage(*staticKeys)
		if err != nil {
			log.Fatal(err)
		}
		storages = append(storages, staticStorage)
	}

	server := wkd.NewServer(storages, wkd.AdvancedSchema)

	log.Printf("starting wkdserver on %s", *addr)
	if err := serverutil.Serve(server, conf.HTTP, *addr); err != nil {
		log.Fatal(err)
	}
}
