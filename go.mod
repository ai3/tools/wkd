module git.autistici.org/ai3/tools/wkd

go 1.15

require (
	git.autistici.org/ai3/go-common v0.0.0-20230816213645-b3aa3fb514d6
	github.com/go-ldap/ldap/v3 v3.4.4
	gopkg.in/yaml.v3 v3.0.1
)
