#!/bin/sh
#
# Given any number of PGP key files as arguments, this script will
# output a JSON file for the wkd server static storage module.
#

tmpdir=$(mktemp -d)
trap "rm -fr ${tmpdir?}" EXIT

set -e

cat "$@" | gpg --homedir $tmpdir --import
mkdir ${tmpdir}/openpgpkeys
gpg --homedir $tmpdir --list-options show-only-fpr-mbox -k '*' \
    | env GNUPGHOME=$tmpdir /usr/lib/gnupg/gpg-wks-client --install-key -C ${tmpdir}/openpgpkeys

echo '{'
gpg --homedir $tmpdir --list-options show-only-fpr-mbox -k '*' \
    | (comma=; while read fpr mbox; do
           hash=$(echo $mbox | /usr/lib/gnupg/gpg-wks-client --print-wkd-hash | awk '{print $1}')
           domain=${mbox##*@}
           echo "${mbox} (${fpr}) -> ${hash}" >&2
           key=$(base64 -w0 < ${tmpdir}/openpgpkeys/${domain}/hu/${hash})
           echo "${comma}  \"${hash}@${domain}\": {\"Addr\": \"${mbox}\", \"Data\": \"${key}\"}"
           comma=,
       done)
echo '}'

exit 0
