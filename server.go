package wkd

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
)

const (
	WELLKNOWN = "/.well-known/openpgpkey/"

	VERSION = 13
)

var ErrNotFound = errors.New("not found")

var errInvalidPath = errors.New("path did not match")

// Request parameters for the WKD protocol.
type Request struct {
	IsPolicy  bool
	LocalPart string
	Domain    string
	Hash      string
}

func (r *Request) Addr() string {
	return fmt.Sprintf("%s@%s", strings.ToLower(r.LocalPart), r.Domain)
}

// SchemaFunc is our request parser function.
type SchemaFunc func(*http.Request) (*Request, error)

// AdvancedSchema implements the 'advanced' URL schema, e.g.:
// https://openpgpkey.example.org/.well-known/openpgpkey/example.org/hu/iy9q119eutrkn8s1mk4r39qejnbu3n5q?l=Joe.Doe
// https://openpgpkey.example.org/.well-known/openpgpkey/example.org/policy
func AdvancedSchema(r *http.Request) (*Request, error) {
	parts := strings.Split(strings.TrimPrefix(r.URL.Path, WELLKNOWN), "/")
	switch {
	case len(parts) == 2 && parts[1] == "policy":
		return &Request{IsPolicy: true}, nil
	case len(parts) == 3 && parts[1] == "hu":
		return &Request{
			LocalPart: r.FormValue("l"),
			Domain:    parts[0],
			Hash:      parts[2],
		}, nil
	default:
		return nil, errInvalidPath
	}
}

// DirectSchema implements the 'direct' URL schema, e.g.:
// https://example.org/.well-known/openpgpkey/hu/iy9q119eutrkn8s1mk4r39qejnbu3n5q?l=Joe.Doe
// https://example.org/.well-known/openpgpkey/policy
func DirectSchema(r *http.Request) (*Request, error) {
	path := strings.TrimPrefix(r.URL.Path, WELLKNOWN)
	if path == "policy" {
		return &Request{IsPolicy: true}, nil
	}
	if strings.HasPrefix(path, "hu/") {
		return &Request{
			LocalPart: r.FormValue("l"),
			Domain:    requestHost(r),
			Hash:      strings.TrimPrefix(path, "hu/"),
		}, nil
	}
	return nil, errInvalidPath
}

// Key data retrieved from storage.
type Key struct {
	Addr string
	Data []byte
}

// Storage interface.
type Storage interface {
	// Lookup a hash in the user database, returning key and user
	// information. The special ErrNotFound error can be used to
	// indicate that no key was found, as opposed to a generic
	// backend error.
	Lookup(context.Context, string, string) (*Key, error)
}

// Server for the WKD protocol.
type Server struct {
	storages []Storage
	schema   SchemaFunc
}

func NewServer(storages []Storage, schema SchemaFunc) *Server {
	return &Server{
		storages: storages,
		schema:   schema,
	}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	if !strings.HasPrefix(r.URL.Path, WELLKNOWN) {
		http.NotFound(w, r)
		return
	}

	req, err := s.schema(r)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	if req.IsPolicy {
		s.servePolicy(w, r)
		return
	}

	s.serveDiscovery(w, r, req)
}

func (s *Server) servePolicy(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	w.Header().Set("Cache-Control", "max-age=604800")
	fmt.Fprintf(w, "protocol-version: %d\n", VERSION)
}

func (s *Server) serveDiscovery(w http.ResponseWriter, r *http.Request, request *Request) {
	var key *Key
	var err error
	var hasErrors bool

	// Go through available storages until one returns a valid key.
	for _, storage := range s.storages {
		key, err = storage.Lookup(r.Context(), request.Hash, request.Domain)
		if err == nil {
			break
		}
		if err != ErrNotFound {
			log.Printf("lookup error (%s): %v", request.Hash, err)
			hasErrors = true
		}
	}

	if key == nil {
		// If we got no results, and we've experienced unexpected
		// backend errors, we'd like to return a 500. The client does
		// not care either way, but by doing so we make the signal
		// more obvious in monitoring.
		if hasErrors {
			http.Error(w, "Backend error", http.StatusInternalServerError)
		} else {
			http.NotFound(w, r)
		}
		return
	}

	// Check that the requested address matches the hash.
	if reqAddr := request.Addr(); reqAddr != key.Addr {
		log.Printf("unauthorized lookup for %s (requested %s, actually %s)", request.Hash, reqAddr, key.Addr)
		http.NotFound(w, r)
		return
	}

	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Length", strconv.Itoa(len(key.Data)))
	w.Header().Set("Cache-Control", "max-age=3600")
	if _, err := w.Write(key.Data); err != nil {
		log.Printf("error writing response to %s: %v", r.RemoteAddr, err)
	}
}

func requestHost(r *http.Request) string {
	if s := r.Header.Get("X-Forwarded-Host"); s != "" {
		return s
	}
	return r.Host
}
