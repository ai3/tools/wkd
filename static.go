package wkd

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
)

// Static, in-memory database of keys, indexed by their WKD hash.
//
// To avoid having to do any GPG parsing in this codebase, we rely on
// the build-static-bundle.sh script to generate data in the necessary
// format using gpg.
//
type staticStorage struct {
	keys map[string]*Key
}

func NewStaticStorage(path string) (Storage, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	var keys map[string]*Key
	if err := json.NewDecoder(f).Decode(&keys); err != nil {
		return nil, err
	}
	return &staticStorage{keys: keys}, nil
}

func (s *staticStorage) Lookup(ctx context.Context, hash, domain string) (*Key, error) {
	hashAddr := fmt.Sprintf("%s@%s", hash, domain)
	if key, ok := s.keys[hashAddr]; ok {
		return key, nil
	}
	return nil, ErrNotFound
}
