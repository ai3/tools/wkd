WKD service
===

This little daemon serves public OpenPGP keys of our users, using the
[WKD](https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/) protocol.

The mechanism has multiple moving parts:

* code in ai3/accountserver to expose and manipulate PGP keys as part
  of the user API ("business logic")
* code in ai3/pannello to let users attach keys to their email
  accounts (UI)
* this server, which implements the WKD protocol
