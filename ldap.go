package wkd

import (
	"context"
	"errors"
	"fmt"
	"strings"

	ldaputil "git.autistici.org/ai3/go-common/ldap"
	"github.com/go-ldap/ldap/v3"
)

const (
	poolSize = 5

	mailLDAPAttr = "mail"
	keyLDAPAttr  = "openPGPKey"
	hashLDAPAttr = "openPGPKeyHash"

	defaultLDAPURI = "ldapi:///var/run/ldap/ldapi"
)

type ldapStorage struct {
	pool *ldaputil.ConnectionPool

	baseDN string
	filter string
}

func NewLDAPStorage(uri, bindDN, bindPw, baseDN, filter string) (Storage, error) {
	if uri == "" {
		uri = defaultLDAPURI
	}
	pool, err := ldaputil.NewConnectionPool(uri, bindDN, bindPw, poolSize)
	if err != nil {
		return nil, err
	}

	if filter == "" {
		filter = fmt.Sprintf("(%s=%%h@%%d)", hashLDAPAttr)
	}
	if !strings.Contains(filter, "%h") {
		return nil, errors.New("filter expression does not contain literal '%h' token")
	}

	return &ldapStorage{
		pool:   pool,
		baseDN: baseDN,
		filter: filter,
	}, nil
}

// Replace '%h' (for hash) and '%d' (for domain) tokens in the
// configured LDAP filter string, and return the result.
func (s *ldapStorage) filterExpr(hash, domain string) string {
	// This is only safe because domain can't contain %h.
	f := strings.Replace(s.filter, "%d", ldap.EscapeFilter(domain), -1)
	return strings.Replace(f, "%h", ldap.EscapeFilter(hash), -1)
}

func (s *ldapStorage) Lookup(ctx context.Context, hash, domain string) (*Key, error) {
	req := ldap.NewSearchRequest(
		s.baseDN,
		ldap.ScopeWholeSubtree,
		ldap.NeverDerefAliases,
		0,
		0,
		false,
		s.filterExpr(hash, domain),
		[]string{mailLDAPAttr, keyLDAPAttr},
		nil,
	)
	result, err := s.pool.Search(ctx, req)
	if err != nil {
		return nil, err
	}

	switch len(result.Entries) {
	case 0:
		return nil, ErrNotFound
	case 1:
		entry := result.Entries[0]
		return &Key{
			Addr: entry.GetAttributeValue(mailLDAPAttr),
			Data: []byte(entry.GetAttributeValue(keyLDAPAttr)),
		}, nil
	default:
		return nil, fmt.Errorf("too many entries returned for hash '%s'", hash)
	}
}
